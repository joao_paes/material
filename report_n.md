# Entrega dos exercícios

- **Grupo**: ds122-2023-2-n
- **Última atualização**: sex 04 ago 2023 09:44:37 -03

|Nome|	ds122-prepare-assignment<br>2023-08-04|
|----|	:---:|
|ADRIANO_DE_OLIVEIRA_BRAGANHOL|	 Fork não encontrado |
|ADRIANO_MONTAGUTI|	 Fork não encontrado |
|ALISSON_GABRIEL_SANTOS|	 ok |
|ANA_BEATRIZ_ROSA_HENRIQUE|	 ok |
|ANDERSON_DA_COSTA_VERBOSKI|	 ok |
|ANDERSON_SOUZA_SANTOS|	 Fork não encontrado |
|ANDRE_DALBERTI_DA_SILVA|	 Fork não encontrado |
|ANDRE_LUIZ_MORAIS_DE_BRITO|	 ok |
|ANDRE_LUIZ_OLMEDO|	 Fork não encontrado |
|ARTHUR_DIAS_BAPTISTA|	 ok |
|AUGUSTO_TONARQUE_LAZZARINO|	 ok |
|BRUNO_CARLOS_DE_SOUZA_BLETES|	 ok |
|CAIO_EDUARDO_MOREIRA_DE_LIMA|	 Fork não encontrado |
|CESAR_EDUARDO_LOPES_BINHARA|	 Fork não encontrado |
|DANIEL_HENRIQUE_BORGES|	 Fork não encontrado |
|EDUARDA_ANTUNES_KLOSS|	 ok |
|EDUARDO_FRANCISCO_DE_MACEDO_KURITZA|	 Fork não encontrado |
|ELIAS_LOPES_DOS_SANTOS|	 Fork não encontrado |
|ENDREWS_MOISES|	 ok |
|ERICK_SANTOS_CAETANO|	 Fork não encontrado |
|ESDRAS_EMANUEL_ALVES_BEZERRA|	 Fork não encontrado |
|FELIPE_EDUARDO_DOS_SANTOS|	 Fork, mas nenhum commit até data de entrega|
|FELIPE_LEITE_TROMBIM|	 ok |
|GABRIEL_ALAMARTINI_TRONI|	 ok |
|GUILHERME_DE_OLIVEIRA_VIOL|	 ok |
|HALYSSON_ASSUNÇÃO_RODRIGUES_JUNIOR|	 Fork não encontrado |
|HELENA_RIGONI_DOS_SANTOS|	 Fork, mas nenhum commit até data de entrega|
|HENRIQUE_SPRENGOSKI_PEREIRA_VAZ|	 Fork não encontrado |
|HENRIQUE_VALDOSKI_ALBA_NATALI|	 ok |
|JOAO_PEDRO_PAES|	 Fork não encontrado |
|LEONARDO_DAVI_PAMPLONA|	 Fork, mas nenhum commit até data de entrega|
|LEONARDO_FELIPE_SALGADO|	 ok |
|LUCAS_GABRIEL_GOMES_DOS_SANTOS|	 Fork não encontrado |
|LUIS_AUGUSTO_GONCALVES_SAWINIEC|	 ok |
|MARIA_TEREZA_MARCHEZAN_FRANK|	 ok |
|MATEUS_BAZAN_BESPALHOK|	 ok |
|MATHEUS_GREGO_DO_AMARAL|	 ok |
|MATHEUS_KUSS_DE_SOUZA|	 Fork não encontrado |
|MATHEUS_NUNES_SANTANA|	 Fork não encontrado |
|MATHEUS_WEGNER_DE_CHRISTO|	 ok |
|MURILO_SANTANA_CARDOSO|	 Fork não encontrado |
|NATHIELI_CAMILE_DE_OLIVEIRA_LEAL|	 Fork não encontrado |
|PATRICK_ALVES_BASTOS|	 ok |
|PEDRO_FELIPE_RIBEIRO_DA_SILVA|	 ok |
|PEDRO_HENRIQUE_DE_SOUZA|	 ok |
|RAFAEL_CECYN_MENDES|	 Fork não encontrado |
|RAFAEL_RODRIGUES_ESTEFANES|	 ok |
|RAUL_FERREIRA_COSTA_BANA|	 ok |
|RICARDO_HENRIQUE_GARCIA_DURIGON|	 Fork não encontrado |
|RODOLFO_COSTACURTA_DOS_SANTOS|	 ok |
|RODRIGO_DO_VALE_STANKOWICZ|	 ok |
|RUPÉLIO_COLFERAI_JUNIOR|	 Fork não encontrado |
|SUSAN_CRISTINI_DE_SOUZA_ALMEIDA|	 Fork não encontrado |
|TIAGO_ÁVILA_JANZ|	 Fork não encontrado |
|TIAGO_PAREJA_VITA|	 ok |
|VINICIUS_EDUARDO_CHACA_FERREIRA|	 Fork não encontrado |
|VIVIANE_ENDO_MORI|	 ok |
|WILLIAM_MATHEUS_DEL_PONTE_FERREIRA|	 Fork não encontrado |
|YASMIN_TAINÁ_DA_SILVA|	 ok |
